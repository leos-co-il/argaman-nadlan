<?php

the_post();
get_header();
$fields = get_fields();
$query = get_queried_object();
$products = get_posts([
    'numberposts' => -1,
    'post_type' => 'product',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        )
    )
]);
$terms = get_terms([
    'taxonomy'      => 'product_cat',
    'hide_empty'    => false,
    'parent'        => 0
]);

?>





<?php get_footer(); ?>