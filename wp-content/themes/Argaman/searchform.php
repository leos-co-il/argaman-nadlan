<?php

$params = [
	['name' => 'מעלית', 'value' => 'lift'],
	['name' => 'גינה', 'value' => 'garden'],
	['name' => 'מחסן', 'value' => 'storage'],
	['name' => 'מרפסת', 'value' => 'balcony'],
	['name' => 'סורגים', 'value' => 'bars'],
	['name' => 'כניסה נפרדת', 'value' => 'sep_entrance'],
	['name' => 'מיזוג אוויר', 'value' => 'air_cond'],
	['name' => 'גישה לנכים', 'value' => 'accessibility'],
	['name' => 'מרפסת כשרה', 'value' => 'cosher_balcony'],
	['name' => 'חניה', 'value' => 'parking'],
	['name' => 'ממ”ד', 'value' => 'dimension'],
	['name' => 'אפשרויות הרחבה', 'value' => 'expansion_options'],
	['name' => 'יחידה נפרדת', 'value' => 'separate_unit'],
	['name' => 'יחידת הורים', 'value' => 'master_unit'],
	['name' => 'נוף', 'value' => 'view'],
];
$cats = get_terms([
	'taxonomy' => 'property_type',
	'hide_empty' => false,
]);
$locations = get_terms([
	'taxonomy' => 'location',
	'hide_empty' => false,
	'parent' => 0
]);

?>

<div class="property-search-section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="property-search">
					<form role="search" method="get" action="" class="search-form box-wrapper property-search-box-wrapper">
						<input name="s" class="d-none">
						<div class="form-row align-items-end">
							<?php if($locations): ?>
							<div class="form-group col-xl col-6">
								<label for="inputAddress">
									<?= esc_html__('עיר | שכונה | רחוב:','leos') ?>
								</label>
								<select name="inputAddress" id="inputAddress">
									<option selected disabled>בחרו:</option>
									<?php foreach($locations as $location): ?>
										<option value="<?= $location->term_id ?>"><?= $location->name ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<?php endif;
							if($cats): ?>
								<div class="form-group col-xl col-6">
									<label for="inputType"><?= esc_html__('סוג נכס','leos') ?></label>
									<select id="inputType" name="property-type" class="form-control">
										<option selected disabled><?= esc_html__('בחרו:','leos'); ?></option>
										<?php foreach($cats as $c): ?>
											<option value="<?= $c->term_id ?>"><?= $c->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl-1 col-md col-6">
								<label for="inputRooms"><?= esc_html__('חדרים','leos') ?></label>
								<div class="input-group">
									<input type="text" class="form-control" name="rooms" id="inputRooms" placeholder="בחרו:">
								</div>
							</div>
							<div class="form-group col-xl-2 col-md col-6 price-group">
								<label for="inputPrice"><?= esc_html__('מחיר:','leos') ?></label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputPrice" name="price-from" placeholder="ממחיר:">
									<input type="text" class="form-control" id="inputPrice2" name="price-to" placeholder="עד מחיר:">
								</div>
							</div>
							<div class="form-group col-xl-auto col-12 d-flex align-items-end">
								<div class="input-group flex-nowrap">
									<button type="button" class="btn btn-advanced">
										<?= esc_html__('חיפוש מתקדם','leos') ?>
										<span class="plus-icon">+</span>
									</button>
									<button type="submit" class="btn btn-search">
										<?php echo esc_html__('חיפוש','leos'); ?>
									</button>
								</div>
							</div>
						</div>
						<div class="advanced-form">
							<h5 class="search-title-group"><?= esc_html__('מאפייני דירה','leos') ?></h5>
							<div class="form-row mb-4">
								<?php if($params): ?>
									<?php foreach($params as $z => $p): ?>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" id="param-<?= $z + 1; ?>" name='params[]' value="<?= $p['value'] ?>">
											<label class="form-check-label" for="inlineCheckbox<?= $z + 1; ?>"><?= $p['name']; ?></label>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<div class="form-row align-items-xl-end">
								<div class="form-group col-xl col-md-4 col-6">
									<label for="inputFloor"><?= esc_html__('קומה','leos') ?></label>
									<div class="input-group">
										<input type="text" class="form-control" name="floor-from" id="inputFloor" placeholder="מקומה:">
										<input type="text" class="form-control" name="floor-to" id="inputFloor2" placeholder="עד קומה:">
									</div>
								</div>
								<div class="form-group col-xl col-md-4 col-6">
									<label for="inputSize"><?= esc_html__('גודל דירה (במ’’ר)','leos') ?></label>
									<div class="input-group">
										<input type="text" class="form-control" name="size-from" id="inputSize" placeholder="מ">
										<input type="text" class="form-control" name="size-to" id="inputSize2" placeholder="עד">
									</div>
								</div>
								<div class="form-groupcol-xl col-md-4 col-6">
									<label for="inputDate"><?= esc_html__('תאריך כניסה','leos') ?></label>
									<input type="date" class="form-control" id="inputDate" name="inputDate">
								</div>
								<div class="col-xl col-6 form-group form-check form-check-inline form-check-inline-cus">
									<input class="form-check-input" type="checkbox" name="property-date-immediately" id="inlineCheckbox1" value="1">
									<label class="form-check-label" for="inlineCheckbox1"><?= esc_html__('כניסה מיידית','leos') ?></label>
								</div>
								<div class="form-group col-xl col-12">
									<button type="submit" class="btn btn-search w-100">
										<?php echo esc_html__('חיפוש','leos'); ?>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
