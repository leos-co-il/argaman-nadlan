<?php

/**
 * The template for displaying Search Results pages.
 */

get_header();
?>
<div class="page-body">
	<div class="container">
		<?php
		$s = get_search_query();
		$_location = (isset($_GET['location'])) ? intval($_GET['location']) : null;
		$_type = (isset($_GET['property-type'])) ? intval($_GET['property-type']) : null;
		$_params = (isset($_GET['params'])) ? $_GET['params'] : null;

		$_room_from = (isset($_GET['rooms'])) ? intval($_GET['rooms']) : 0;

		$_price_from = (isset($_GET['price-from'])) ? intval($_GET['price-from']) : 0;
		$_price_to = (isset($_GET['price-to']) && !empty($_GET['price-to'])) ? intval($_GET['price-to']) : PHP_INT_MAX;


		$_floor_from = (isset($_GET['floor-from'])) ? intval($_GET['floor-from']) : 0;
		$_floor_to = (isset($_GET['floor-to']) && !empty($_GET['floor-to'])) ? intval($_GET['floor-to']) : PHP_INT_MAX;

		$_size_from = (isset($_GET['size-from'])) ? intval($_GET['size-from']) : 0;
		$_size_to = (isset($_GET['size-to']) && !empty($_GET['size-to'])) ? intval($_GET['size-to']) : PHP_INT_MAX;

		$_date = (isset($_GET['property-date'])) ? $_GET['property-date'] : null;

		$_immediately = (isset($_GET['property-date-immediately'])) ? 1 : null;

		$query_args = [
			'post_type' => 'property',
			'posts_per_page' => -1,
			's' => $s,
			'meta_query' => [
				[
					'key' => 'room',
					'value'   => [$_room_from],
					'type'    => 'numeric',
					'compare' => '='
				],
				[
					'key' => 'price',
					'value'   => [$_price_from, $_price_to],
					'type'    => 'numeric',
					'compare' => 'BETWEEN',
				],
				[
					'key' => 'floor',
					'value'   => [$_floor_from, $_floor_to],
					'type'    => 'numeric',
					'compare' => 'BETWEEN',
				],
				[
					'key' => 'square',
					'value'   => [$_size_from, $_size_to],
					'type'    => 'numeric',
					'compare' => 'BETWEEN',
				],
			]
		];

		if($_immediately){
			array_push($query_args['meta_query'], [
				'key' => 'free_date_bool',
				'value' => '1',
				'compare' => '=='
			]);
		}elseif($_date){
			array_push($query_args['meta_query'], [
				'key' => 'free_date',
				'value' => $_date,
				'compare' => '>=',
				'type' => 'DATE'
			]);
		}
		if($_location || $_type){
			$query_args['tax_query'] = [
				'relation' => 'AND',
				$_location ? [
					'taxonomy' => 'location',
					'field'    => 'term_id',
					'terms'    => [$_location],
				] : null,
				$_type ? [
					'taxonomy' => 'property_type',
					'field'    => 'term_id',
					'terms'    => [$_type],
				] : null,
			];
		}else {
			$query_args['tax_query'] = null;
		}
		$properties = new WP_Query($query_args);
		if ($properties->have_posts() ) { ?>
		<div class="row justify-content-center">
			<div class="col-auto">
				<h4 class="block-title mb-4">
					<?= esc_html__('תוצאות חיפוש','leos');?>
				</h4>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $properties->have_posts() ) { $properties->the_post();
				$id = get_the_ID();
				$link = get_the_permalink();
				$rooms = get_field('rooms', $id);
				$size = get_field('size', $id);
				$price = get_field('price', $id);
				$free = get_field('free', $id);?>
				<div class="property-card-col property-card-list">
					<a class="property-card-img" href="<?= $link; ?>"
						<?php if (has_post_thumbnail()) : ?>
							style="background-image: url('<?= postThumb(); ?>')"
						<?php endif;?>>
						<?php if ($free) : ?>
							<span class="tip-pro">
						ללא עמלת
תיווך!
					</span>
						<?php endif; ?>
						<div class="property-card-overlay">
							<div class="property-title-wrap">
								<h6 class="property-title"><?php the_title(); ?></h6>
							</div>
						</div>
					</a>
					<div class="property-item-content">
						<ul class="row">
							<?php if ($rooms) : ?>
								<li class="col-auto li-value-prop">
									<strong>חדרים:</strong><?= ' '.$rooms; ?>
								</li>
							<?php endif;
							if ($size) : ?>
								<li class="col-auto li-value-prop">
									<strong>גודל:</strong><?= ' '.$size.' מ”ר'; ?>
								</li>
							<?php endif;
							if ($price) : ?>
								<li class="col-auto li-value-prop">
									<strong>מחיר:</strong><?= ' '.$price.' ש”ח'; ?>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="col-12 pt-5">
					<h4 class="block-title">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
