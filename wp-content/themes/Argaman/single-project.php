<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$postId = get_the_ID();
//For project
$title = 'פרויקטים נוספים שעשויים לעניין אתכם';
$share_title = 'שתפו פרוייקט זה:';
//End for project
$post_gallery = $fields['post_gallery'];;
$title = $fields['same_props_title'];
$samePosts = [];
$post_terms = wp_get_object_terms($postId, 'project_cat', ['fields' => 'ids']);
$values = [
	['name' => 'כמות דירות', 'value' => $fields['flats']],
	$fields['master_unit'] ? ['name' => '', 'value' => 'יחידת הורים'] : '',
	['name' => 'סוגי דירות', 'value' => $fields['type_apt']],
	['name' => 'שטח הנכס', 'value' => $fields['size']],
	['name' => 'טווח מחירים:', 'value' => $fields['currency'] ? $fields['currency'] : 'ש"ח'],
	['name' => 'מפרסות', 'value' => $fields['balcony'] ? 'אפשרויות הרחבה' : ''],
	['name' => 'קומה:', 'value' => $fields['floor']],
	['name' => 'תאריך פינוי:', 'value' => $fields['free_date']],
];
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container article-border">
		<div class="row mob-prop-title">
			<div class="col-12">
				<h1 class="block-title text-right mb-5"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<h1 class="block-title desc-prop-title text-right mb-5"><?php the_title(); ?></h1>
				<?php if ($values) : ?>
					<div class="property-values">
						<?php if ($values) : ?>
							<ul class="row">
								<?php foreach ($values as $value) : if ($value['value']) : ?>
									<li class="col-xl-6 col-lg-12 col-sm-6 col-12 li-value-prop">
										<?= $value['name'].' '.$value['value']; ?>
									</li>
								<?php endif; endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['project_map']) : ?>
					<div class="property-map">
						<h5 class="share-title">
							מפת האזור:
						</h5>
						<?= $fields['project_map']; ?>
					</div>
				<?php endif; ?>
				<div class="socials-share-block">
					<h5 class="share-title">
						<?= $share_title; ?>
					</h5>
					<div class="socials-share-line">
						<!--	WHATSAPP-->
						<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
							<img src="<?= ICONS ?>whatsapp-share.png">
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank" class="social-share-link">
							<img src="<?= ICONS ?>facebook-share.png">
						</a>
						<!--	MAIL-->
						<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank" class="social-share-link">
							<img src="<?= ICONS ?>mail-share.png">
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-12 product-gallery-col">
				<?php if ($post_gallery || has_post_thumbnail()) : ?>
					<div class="arrows-slider arrows-slider-base post-gallery-block">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							if ($post_gallery) : foreach ($post_gallery as $img): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; endif; ?>
						</div>
						<?php if ($post_gallery) : ?>
							<div class="thumbs-wrap">
								<div class="thumbs" dir="rtl">
									<?php if(has_post_thumbnail()): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
											   href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
										</div>
									<?php endif; foreach ($post_gallery as $img): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= $img['url']; ?>')"
											   href="<?= $img['url']; ?>" data-lightbox="images-small">
											</a>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="put-arrows-here"></div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($fields['owner_desc'] || $fields['owner_whatsapp'] || $fields['owner_tel']) :
					$name = isset($fields['owner_name']) ? $fields['owner_name'] : 'סוכן של נכס זה';?>
					<div class="owner-block">
						<div class="row align-items-center">
							<div class="<?= $fields['owner_img'] ? 'col-xl-6 col-lg-12 col-sm-6 col-12' : 'col-12'; ?>">
								<h5 class="share-title">
									צרו קשר עם הסוכן של פרוייקט זה
								</h5>
								<?php if ($fields['owner_desc']) : ?>
									<p class="owner-text">
										<?= $fields['owner_desc']; ?>
									</p>
								<?php endif;
								if ($fields['owner_whatsapp']) : ?>
									<a href="https://api.whatsapp.com/send?phone=<?= $fields['owner_whatsapp']; ?>"
									   class="owner-link">
										<img src="<?= ICONS ?>owner-whatsapp.png" alt="whatsapp">
										<?= 'וואטסאפ ל'.$name; ?>
									</a>
								<?php endif;
								if ($fields['owner_tel']) : ?>
									<a href="tel:<?= $fields['owner_tel']; ?>"
									   class="owner-link">
										<img src="<?= ICONS ?>owner-tel.png" alt="phone">
										<?= 'חייגו ל'.$name; ?>
									</a>
								<?php endif; ?>
							</div>
							<?php if ($fields['owner_img']) : ?>
								<div class="col-xl-6 col-lg-12 col-sm-6 col-12 mt-xl-0 mt-lg-4 mt-sm-0 mt-4 owner-img-col">
									<div class="owner-img">
										<span class="owner-overlay">
										</span>
										<img src="<?= $fields['owner_img']['url']; ?>">
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'project',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_prods']) {
	$samePosts = $fields['same_prods'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'project',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="home-posts-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title">
						<?= $title; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'project',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>
