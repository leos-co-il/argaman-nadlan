<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container article-border">
		<div class="row">
			<div class="col-12">
				<h1 class="block-title mb-5"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="floated-container">
					<div class="floated-col">
						<?php if (has_post_thumbnail()) : ?>
							<div class="main-post-image">
								<img src="<?= postThumb(); ?>" alt="post-image">
							</div>
						<?php endif; ?>
						<div class="post-form-wrap">
							<?php if ($title = opt('post_form_title')) : ?>
								<h2 class="post-form-title"><?= $title; ?></h2>
							<?php endif;
							if ($subtitle = opt('post_form_subtitle')) : ?>
								<h3 class="post-form-subtitle"><?= $subtitle; ?></h3>
							<?php endif; ?>
							<div class="base-form-wrap">
								<?php getForm('45'); ?>
							</div>
						</div>
					</div>
					<div class="base-output post-output">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$type = get_post_type();
$samePosts = [];
$title = $fields['same_title'];
$link = $fields['same_link'] ? $fields['same_link'] : opt('blog_link');
if ($type === 'post') {
	$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => 'post',
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
	$title = 'מאמרים נוספים שיעניינו אותך';
} else {
	$title = 'שירותים נוספים שיעניינו אותך';
	$link = $fields['same_link'] ? $fields['same_link'] : opt('blog_link');
}
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => $type,
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="home-posts-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title">
						<?= $title; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) : ?>
					<div class="col-xl-3 col-md-6 col-12 mb-4">
						<?php get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-end mt-3">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>
