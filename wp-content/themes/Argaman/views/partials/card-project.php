<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']);
	$flats = get_field('flats', $args['post']->ID);
	$types = get_field('types_prop', $args['post']->ID);
	$price = get_field('price', $args['post']->ID);
	$free = get_field('free', $args['post']->ID); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-3 project-item position-relative">
		<?php if ($free) : ?>
			<span class="tip-pro">
						ללא עמלת
תיווך!
					</span>
		<?php endif; ?>
		<div class="property-card-col property-card-list more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="property-card-img" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
				<div class="property-card-overlay"></div>
			</a>
			<div class="property-title-wrap">
				<a class="property-title mb-2" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<div class="property-item-content">
					<ul class="row">
						<?php if ($flats) : ?>
							<li class="col-auto li-value-prop">
								<?= 'כמות דירות בפרוייקט: '.$flats; ?>
							</li>
						<?php endif;
						if ($types) : ?>
							<li class="col-auto li-value-prop">
								<?= 'סוגי נכסים: ';
								foreach ($types as $type) {
									echo $type->name.', ';
								} ?>
							</li>
						<?php endif;
						if ($price) : ?>
							<li class="col-auto li-value-prop">
								<?= 'טווח מחיר: החל מ- '.$price; ?>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
