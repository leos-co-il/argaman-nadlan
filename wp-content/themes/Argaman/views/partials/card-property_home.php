<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']);
$rooms = get_field('rooms', $args['post']->ID);
$size = get_field('size', $args['post']->ID);
$price = get_field('price', $args['post']->ID);
	$free = get_field('free', $args['post']->ID);?>
	<div class="property-card-col property-card-list">
		<a class="property-card-img" href="<?= $link; ?>"
			<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif;?>>
			<?php if ($free) : ?>
				<span class="tip-pro">
						ללא עמלת
תיווך!
					</span>
			<?php endif; ?>
			<div class="property-card-overlay">
				<div class="property-title-wrap">
					<h6 class="property-title"><?= $args['post']->post_title; ?></h6>
				</div>
			</div>
		</a>
		<div class="property-item-content">
			<ul class="row">
				<?php if ($rooms) : ?>
					<li class="col-auto li-value-prop">
						<strong>חדרים:</strong><?= ' '.$rooms; ?>
					</li>
				<?php endif;
				if ($size) : ?>
					<li class="col-auto li-value-prop">
						<strong>גודל:</strong><?= ' '.$size.' מ”ר'; ?>
					</li>
				<?php endif;
				if ($price) : ?>
					<li class="col-auto li-value-prop">
						<strong>מחיר:</strong><?= ' '.$price.' ש”ח'; ?>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
