<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-md-6 col-12 post-col service-col">
		<div class="post-item service-item more-card" data-id="<?= $args['post']->ID; ?>">
			<div class="card-img post-item-image"<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif;?>>
				<a class="service-overlay" href="<?= $link; ?>">
					<h3 class="service-title"><?= $args['post']->post_title; ?></h3>
					<span class="post-item-link service-item-link">
						לצפייה
					</span>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
