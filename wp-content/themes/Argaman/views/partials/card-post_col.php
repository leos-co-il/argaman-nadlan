<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-sm-6 col-12 post-col">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="card-img post-item-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-item-content">
				<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<a class="post-item-link" href="<?= $link; ?>">
					להמשך קריאה
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
