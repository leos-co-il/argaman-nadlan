<section class="repeat-form-block" <?php if ($img = opt('base_form_back')) : ?>
	style="background-image: url('<?= $img['url']; ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center form-wrapper">
			<div class="col-xl-10 col-sm-11 col-12">
				<?php if ($title = opt('base_form_title')) : ?>
					<h2 class="base-form-title"><?= $title; ?></h2>
				<?php endif;
				if ($subtitle = opt('base_form_subtitle')) : ?>
					<h3 class="base-form-subtitle"><?= $subtitle; ?></h3>
				<?php endif; ?>
				<?php getForm('40'); ?>
			</div>
		</div>
	</div>
</section>
