<?php
/*
Template Name: אודות
*/

get_header();
the_post();
$fields = get_fields();
$img = has_post_thumbnail() ? postThumb() : '';
?>


<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="about-page-body">
		<?php if ($img) : ?>
			<div class="about-img">
				<img src="<?= $img; ?>" alt="about-img">
			</div>
		<?php endif; ?>
		<div class="container">
			<div class="row">
				<div class="<?= $img ? 'col-xl-6 col-lg-8' : '' ; ?>">
					<div class="base-output">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="<?= $img ? 'col-xl-6 col-lg-4' : '' ; ?>">
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['team_item']) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= $fields['tem_title'] ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-lg-between justify-content-center align-items-stretch mb-5">
				<?php foreach ($fields['team_item'] as $x => $worker) : ?>
					<div class="col-lg-4 col-sm-6 col-12 worker-col-item">
						<div class="worker-item">
							<div class="worker-img" <?php if ($worker['team_img']) : ?>
									style="background-image: url('<?= $worker['team_img']['url']; ?>')"
								<?php endif; ?>>
								<div class="owner-overlay">
									<div class="worker-info-wrap mt-minus">
										<h5 class="worker-name"><?= $worker['team_name']; ?></h5>
										<p class="worker-position"><?= $worker['team_desc']; ?></p>
									</div>
								</div>
							</div>
							<?php if ($worker['team_whatsapp']) : ?>
								<a href="https://api.whatsapp.com/send?phone=<?= $worker['team_whatsapp']; ?>"
								   class="owner-link">
									<img src="<?= ICONS ?>owner-whatsapp.png" alt="whatsapp">
									<?= 'וואטסאפ ל'.$worker['team_name']; ?>
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
