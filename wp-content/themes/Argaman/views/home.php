<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>
<section class="main-block">
	<div class="padding-block">
		<div class="main-slider-wrap">
			<div class="main-slider" dir="rtl">
				<?php if($fields['home_slider']) : foreach ($fields['home_slider'] as $item) : ?>
					<div class="slide-main" style="background-image: url('<?= $item['url']; ?>')">
						<div class="slide-main-overlay"></div>
					</div>
				<?php endforeach; endif; ?>
			</div>
		</div>
	</div>
	<div class="content-main-wrap">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10 col-12">
					<?php if ($logo_white = opt('logo_white')) : ?>
						<div class="row justify-content-center">
							<div class="col-md-4 col-5">
								<a href="/" class="logo logo-home">
									<img src="<?= $logo_white['url'] ?>" alt="logo">
								</a>
							</div>
						</div>
					<?php endif;
					if ($fields['h_title']) : ?>
						<h2 class="base-form-title"><?= $fields['h_title']; ?></h2>
					<?php endif;
					if ($fields['h_subtitle']) : ?>
						<h3 class="base-form-subtitle"><?= $fields['h_subtitle']; ?></h3>
					<?php endif; ?>
					<?php getForm('43'); ?>
				</div>
			</div>
		</div>
		<div class="search-home">
			<?php if ($fields['h_search_title']) : ?>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col">
							<h2 class="search-title-home">
								<?= $fields['h_search_title']; ?>
							</h2>
						</div>
					</div>
				</div>
			<?php endif;
			get_search_form(); ?>
		</div>
	</div>
</section>

<!--Properties-->
<?php if ($fields['h_props_slider']) :  ?>
	<section class="home-posts-block arrows-slider">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title">
						<?= $fields['h_props_title']; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-12">
					<div class="properties-slider" dir="rtl">
						<?php foreach ($fields['h_props_slider'] as $post) : ?>
							<div class="p-2">
								<?php get_template_part('views/partials/card', 'property_home',
										[
												'post' => $post,
										]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_projects']) :
	$link = (isset($fields['h_projects_link']['url']) && $fields['h_projects_link']) ? $fields['h_projects_link'] : ''; ?>
	<section class="home-posts-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title">
						<?= $fields['h_projects_title']; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_projects'] as $post) {
					get_template_part('views/partials/card', 'project',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-end mt-4">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'לכל הפרויקטים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['counter_item']) : ?>
	<div class="counter-block">
		<div class="container" id="start-count">
			<div class="row justify-content-center">
				<?php foreach ($fields['counter_item'] as $count) : ?>
					<div class="col-md-3 col-6 count-col mb-md-0 mb-5">
						<div class="counter-item">
							<h3 class="timer counter-number counter-num" data-from="0"
								data-to="<?= intval($count['num']); ?>" data-speed="1500">
							</h3>
							<h4 class="counter-text"><?= $count['title']; ?></h4>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif;?>
<!--About-->
<section class="home-about-block">
	<?php if ($fields['h_about_text'] || $fields['h_about_link']) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10 col-sm-11 col-12">
					<?php if ($fields['h_about_title']) : ?>
						<h2 class="block-title">
							<?= $fields['h_about_title']; ?>
						</h2>
					<?php endif; ?>
					<div class="base-output text-center">
						<?=  $fields['h_about_text']; ?>
					</div>
				</div>
			</div>
			<?php if ($link = (isset($fields['h_about_link']['url']) && $fields['h_about_link']) ? $fields['h_about_link'] : '') : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'להמשך קריאה'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endif;
	if ($fields['h_about_img']) : ?>
		<div class="about-img-home-wrap">
			<img src="<?= $fields['h_about_img']['url']; ?>" alt="home-about" class="about-img-home">
		</div>
	<?php endif;
	get_template_part('views/partials/repeat', 'form'); ?>
</section>
<?php if ($fields['h_services']) :
	$link = (isset($fields['h_serv_link']['url']) && $fields['h_serv_link']) ? $fields['h_serv_link'] : ''; ?>
	<section class="services-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title">
						<?= $fields['h_serv_title']; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_services'] as $post) {
					get_template_part('views/partials/card', 'service_home',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'מעבר לכל השירותים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['h_review']) : ?>
	<section class="reviews-block arrows-slider arrows-slider-white" <?php if ($back_revs = $fields['h_revs_back']) : ?>
		style="background-image: url('<?= $back_revs['url']; ?>')"
	<?php endif; ?>>
		<div class="container">
			<?php if ($fields['h_revs_block_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title block-title-white"><?= $fields['h_revs_block_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($fields['h_review'] as $x => $review) : ?>
							<div class="review-slide">
								<div class="review-item">
									<span class="quote quote-top">	&#8221</span>
									<span class="quote quote-bottom">	&#8221</span>
									<div class="rev-content">
										<div class="review-logo">
											<?php if ($review['rev_logo']) : ?>
												<img src="<?= $review['rev_logo']['url']; ?>" alt="logo-review">
											<?php elseif ($logo = opt('logo')) : ?>
												<img src="<?= $logo['url']; ?>" alt="logo-review">
											<?php endif; ?>
										</div>
										<h3 class="review-title"><?= $review['rev_name']; ?></h3>
										<p class="base-text text-center">
											<?= $review['rev_text']; ?>
										</p>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_posts']) :
	$link = (isset($fields['h_posts_link']['url']) && $fields['h_posts_link']) ? $fields['h_posts_link'] : ''; ?>
	<section class="home-posts-block arrows-slider arrows-slider-post">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title">
						<?= $fields['h_posts_title']; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-12">
					<div class="posts-slider" dir="rtl">
						<?php foreach ($fields['h_posts'] as $post) : ?>
							<div class="p-2">
								<?php get_template_part('views/partials/card', 'post',
										[
												'post' => $post,
										]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
