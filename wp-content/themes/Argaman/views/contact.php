<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$tel_2 = opt('tel_2');
$mail = opt('mail');
$mail_2 = opt('mail_2');
$address = opt('address');
$fax = opt('fax');
$map = opt('map_image');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title mb-5">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-12">
				<div class="row justify-content-center">
					<?php if ($address) : ?>
						<div class="contact-item-link contact-item col-md-4 col-sm-6 col-12 wow zoomIn" data-wow-delay="0.2s">
							<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</div>
								<h3 class="contact-item-title">
									בואו לקפה!
								</h3>
								<p class="contact-item-title-info">
									<?= $address; ?>
								</p>
							</a>
						</div>
					<?php endif;
					if ($tel) : ?>
						<div class="col-md-4 col-sm-6 col-12 contact-item contact-item-link wow zoomIn" data-wow-delay="0.4s">
							<a href="tel:<?= $tel; ?>" class="contact-info">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<h3 class="contact-item-title">
									תרימו טלפון!
								</h3>
								<p class="contact-item-title-info">
									<?= $tel; ?>
								</p>
							</a>
						</div>
					<?php endif;
					if ($mail) : ?>
						<div class="contact-item col-md-4 col-sm-6 col-12 contact-item-link wow zoomIn" data-wow-delay="0.6s">
							<a href="mailto:<?= $mail; ?>" class="contact-info">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<h3 class="contact-item-title">
									אפשר גם במייל!
								</h3>
								<p class="contact-item-title-info">
									<?= $mail; ?>
								</p>
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-12">
				<div class="contact-form-page">
					<?php if ($fields['contact_form_title']) : ?>
						<h2 class="contact-form-title">
							<?= $fields['contact_form_title']; ?>
						</h2>
					<?php endif;
					if ($fields['contact_form_subtitle']) : ?>
						<h3 class="contact-form-subtitle">
							<?= $fields['contact_form_subtitle']; ?>
						</h3>
					<?php endif;
					getForm('42'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
