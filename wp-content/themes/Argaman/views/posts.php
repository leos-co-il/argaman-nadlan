<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['type'] ? $fields['type'] : 'post';
$more = '';
$type_card = 'post_col';
$quantity = 6;
switch ($type) {
	case 'post' :
		$more = 'לחצו למאמרים נוספים';
		break;
	case 'project' :
		$more = 'לחצו לפרויקטים נוספים';
		$type_card = 'project';
		$quantity = 8;
		break;
	case 'service' :
		$more = 'לחצו לשירותים נוספים';
		$type_card = 'service';
		break;
	case 'property' :
		$more = 'לחצו לנכסים נוספים';
		$type_card = 'property';
		$quantity = 8;
		break;
}
$posts = new WP_Query([
    'posts_per_page' => $quantity,
    'post_type' => $type,
    'suppress_filters' => false
]);
$published_posts = new WP_Query([
    'posts_per_page' => -1,
    'post_type' => $type,
    'suppress_filters' => false,
]);
?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
                    <?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
                    <?php the_content(); ?>
				</div>
			</div>
		</div>
        <?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
                <?php foreach ($posts->posts as $post) {
                    get_template_part('views/partials/card', $type_card,
                        [
                            'post' => $post,
                        ]);
                } ?>
			</div>
        <?php endif;
        if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > $quantity)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="base-link more-link load-more-posts" data-type="<?= $type; ?>" data-count="<?= $num; ?>">
                        <?= $more ? $more : 'טען עוד'; ?>
						<img src="<?= ICONS ?>arrow-link.png" alt="load-more">
					</div>
				</div>
			</div>
        <?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
    get_template_part('views/partials/content', 'slider', [
        'content' => $fields['single_slider_seo'],
        'img' => $fields['slider_img'],
    ]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

