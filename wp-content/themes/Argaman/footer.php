<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$facebook = opt('facebook');

$locations = get_nav_menu_locations();
$menu_obj = wp_get_nav_menu_object($locations['footer-links-menu']);
$links_menu_title = get_field('menu_title', $menu_obj);
$menu_obj_main = wp_get_nav_menu_object($locations['footer-menu']);
$foo_menu_title = get_field('menu_title', $menu_obj_main);
$menu_obj_links_2 = wp_get_nav_menu_object($locations['footer-links-menu']);
$foo_links_title_2 = get_field('menu_title', $menu_obj_links_2);
$menu_obj_services = wp_get_nav_menu_object($locations['footer-services-menu']);
$foo_services_title = get_field('menu_title', $menu_obj_services);
$back = opt('foo_back');
?>
<footer>
	<div class="footer-main-back" <?php if ($back) : ?>
		style="background-image: url('<?= $back['url']; ?>')"
	<?php endif; ?>>
		<div class="footer-main" <?php if (!$back) : ?> style="background: transparent" <?php endif; ?>>
			<a id="go-top">
				<img src="<?= ICONS ?>to-top.png" alt="to-top">
				<span class="top-text">
				חזרה
				למעלה
			</span>
			</a>
			<div class="container">
				<?php if ($logo = opt('foo_logo')) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<a href="/" class="logo logo-foo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<div class="col-xl-10 col-sm-11 col-12 footer-form">
						<?php if ($f_title = opt('foo_form_title')) : ?>
							<h2 class="foo-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('foo_form_subtitle')) : ?>
							<h3 class="base-form-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						getForm('41'); ?>
						<ul class="row contact-list">
							<?php if ($address) : ?>
								<li class="col-lg-3 col-6 foo-contact-col">
									<a href="https://waze.com/ul?q=<?= $address; ?>" target="_blank">
									<span class="foo-contact-icon">
										<img src="<?= ICONS ?>foo-geo.png" alt="geo">
									</span>
										<span class="contact-foo-title">
										בקרו אותנו
									</span>
										<span class="contact-info-footer">
										<?= $address; ?>
									</span>
									</a>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li class="col-lg-3 col-6 foo-contact-col">
									<a href="mailto:<?= $mail; ?>">
									<span class="foo-contact-icon">
										<img src="<?= ICONS ?>foo-mail.png" alt="email">
									</span>
										<span class="contact-foo-title">
									דברו איתנו
									</span>
										<span class="contact-info-footer">
										<?= $mail; ?>
									</span>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li class="col-lg-3 col-6 foo-contact-col">
									<a href="tel:<?= $tel; ?>">
									<span class="foo-contact-icon">
										<img src="<?= ICONS ?>foo-tel.png" alt="phone-number">
									</span>
										<span class="contact-foo-title">
									דברו איתנו
									</span>
										<span class="contact-info-footer">
										<?= $tel; ?>
									</span>
									</a>
								</li>
							<?php endif;
							if ($facebook) : ?>
								<li class="col-lg-3 col-6 foo-contact-col">
									<a href="<?= $facebook?>">
									<span class="foo-contact-icon">
										<img src="<?= ICONS ?>foo-facebook.png" alt="facebook">
									</span>
										<span class="contact-foo-title">
										עשו לנו לייק
									</span>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="container footer-container-menu">
				<div class="row justify-content-sm-between justify-content-center align-items-stretch foo-after-title">
					<div class="col-lg-auto col-sm-6 col-12 foo-menu foo-main-menu">
						<h3 class="foo-title">
							<?= $foo_menu_title ? $foo_menu_title : esc_html__('ניווט מהיר', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-menu', '1'); ?>
						</div>
					</div>
					<div class="col-lg-auto col-sm-6 col-12 foo-menu foo-services-menu">
						<h3 class="foo-title">
							<?= $foo_services_title ? $foo_services_title : esc_html__('השירותים שלנו', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-services-menu', '1'); ?>
						</div>
					</div>
					<div class="col-lg-auto col-sm-6 col-12 foo-menu foo-links-menu-2">
						<div>
							<h3 class="foo-title">
								<?= $links_menu_title ? $links_menu_title : esc_html__('חם מהבלוג', 'leos'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu', '1'); ?>
							</div>
						</div>
					</div>
					<div class="col-lg-auto col-sm-6 col-12 foo-menu foo-links-menu-2">
						<div>
							<h3 class="foo-title">
								<?= $foo_links_title_2 ? $links_menu_title : esc_html__('פרויקטים רלוונטים', 'leos'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu-2', '1'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
