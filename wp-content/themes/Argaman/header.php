<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header class="sticky <?= is_front_page() ? 'home-header' : '';?>">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-sm-auto col-7">
				<?php if ($logo = opt('logo_header')) : ?>
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				<?php endif; ?>
            </div>

            <div class="col">
            <nav id="MainNav" class="h-100">
                    <div id="MobNavBtn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
                </nav>
            </div>
			<div class="col-auto row-contacts-header">
				<div class="row justify-content-end align-items-center">
					<div class="col d-flex align-items-center">
						<?php if ($whatsapp = opt('whatsapp')) : ?>
							<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>"
							   class="whatsapp-link">
								<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
							</a>
						<?php endif;
						if ($tel = opt('tel')) : ?>
							<a href="tel:<?= $tel; ?>" class="header-tel-link">
								<img src="<?= ICONS ?>header-phone.png" alt="phone">
								<span><?= $tel; ?></span>
							</a>
						<?php endif; ?>
					</div>
					<div class="col-auto search-trigger-col">
						<div class="search-trigger">
							<span>לחצו לחיפוש</span>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</header>

<section class="pop-search">
	<span class="close-search">X</span>
	<?php get_search_form(); ?>
</section>

<div class="fixed-links">
	<div class="fixed-link pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="open-popup">
	</div>
	<?php if ($facebook = opt('facebook')) : ?>
		<a class="fixed-link facebook-link" href="<?= $facebook; ?>">
			<img src="<?= ICONS ?>facebook.png" alt="to-facebook">
		</a>
	<?php endif;
	if ($linkedin = opt('linkedin')) : ?>
		<a class="fixed-link linkedin-link" href="<?= $linkedin; ?>">
		<img src="<?= ICONS ?>linkedin.png" alt="to-linkedin">
		</a>
	<?php endif; ?>
</div>
